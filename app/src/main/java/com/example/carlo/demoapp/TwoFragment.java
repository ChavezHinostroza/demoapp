package com.example.carlo.demoapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class TwoFragment extends Fragment   {

    private RecyclerView mProductList;
    private CardView blanco;
    private LinearLayoutManager mProdLayoutManager;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseBusqueda;
    private DatabaseReference mDatabaseLike;
    private DatabaseReference mDatabaseUsers;
    private FirebaseAuth mAuth;

    //GRID

    private GridLayoutManager mItemsLayoutManager;

    private EditText search_edit_text;
    FirebaseRecyclerAdapter<Producto, TwoFragment.ProductoViewHolder> firebaseRecyclerAdapter;
    private ArrayList<String> pNameList;
    private ArrayList<String> pPrecioList;
    private ArrayList<String> pProcedenciaList;
    private ArrayList<String> pFechaList;
    private ArrayList<String> pCantidadList;
    private ArrayList<String> pTipoList;
    private ArrayList<String> pImageList;
    private ArrayList<String> pKeyList;

    private SearchAdapter searchAdapter;
    private FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseUser firebaseUser;

    public TwoFragment() {
        // Required empty public constructor
    }

    //Like procces
    private boolean mProcessLike=false;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.fragment_two, container, false);



        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent Loginintent = new Intent(getActivity(),ActivityRegister2.class);
                    Loginintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(Loginintent);

                }
            }
        };

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
        mDatabaseBusqueda = FirebaseDatabase.getInstance().getReference();
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("users2");
        mDatabaseLike=FirebaseDatabase.getInstance().getReference().child("Likes");




        search_edit_text=(EditText) vista.findViewById(R.id.search_edit_text) ;

        mProductList= (RecyclerView) vista.findViewById(R.id.productlist2);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new GridLayoutManager(getContext(),2));

  /*
        * Create a array list for each node you want to use
        * */
         pNameList= new ArrayList<>();
         pPrecioList= new ArrayList<>();
         pProcedenciaList= new ArrayList<>();
         pFechaList= new ArrayList<>();
         pCantidadList= new ArrayList<>();
         pTipoList= new ArrayList<>();
         pImageList= new ArrayList<>();
         pKeyList=new ArrayList<>();





        search_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                } else {


                    // Clear the list when editText is empty

                  pNameList.clear();
                    pPrecioList.clear();
                    pCantidadList.clear();
                    pProcedenciaList.clear();
                    pFechaList.clear();
                    pTipoList.clear();
                    pImageList.clear();
                    pKeyList.clear();

                mProductList.removeAllViews();


                }
            }
        });

        checkUserExist();
        return vista;
    }


    private void setAdapter(final String searchedString) {
        mDatabaseBusqueda.child("Producto").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                /*
                * Clear the list for every new search
                * */
                pNameList.clear();
                pPrecioList.clear();
                pCantidadList.clear();
                pProcedenciaList.clear();
                pFechaList.clear();
                pTipoList.clear();
                pImageList.clear();
                pKeyList.clear();
                mProductList.removeAllViews();

                int counter = 0;

                /*
                * Search all products for matching searched string
                * */
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    //String uid = snapshot.getKey();
                    String prodname = snapshot.child("prodname").getValue(String.class);
                    String precio = snapshot.child("precio").getValue(String.class);
                    String cantidad = snapshot.child("cantidad").getValue(String.class);
                    String fechaor = snapshot.child("fechaor").getValue(String.class);
                    String procedencia = snapshot.child("procedencia").getValue(String.class);
                    String tipoprod = snapshot.child("tipoprod").getValue(String.class);
                    String imageprod = snapshot.child("imageprod").getValue(String.class);
                    String keyprod = snapshot.getKey();


                    if (prodname.toLowerCase().contains(searchedString.toLowerCase())) {
                        pNameList.add(prodname);
                        pPrecioList.add(precio);
                        pCantidadList.add(cantidad);
                        pProcedenciaList.add(procedencia);
                        pFechaList.add(fechaor);
                        pTipoList.add(tipoprod);
                        pImageList.add(imageprod);
                        pKeyList.add(keyprod);
                        counter++;
                    } else if (tipoprod.toLowerCase().contains(searchedString.toLowerCase())) {
                        pNameList.add(prodname);
                        pPrecioList.add(precio);
                        pCantidadList.add(cantidad);
                        pProcedenciaList.add(procedencia);
                        pFechaList.add(fechaor);
                        pTipoList.add(tipoprod);
                        pImageList.add(imageprod);
                        pKeyList.add(keyprod);
                        counter++;
                    }

                    /*
                    * Get maximum of 15 searched results only
                    * */
                    if (counter == 15)
                        break;
                }

                searchAdapter = new SearchAdapter(getActivity().getApplicationContext(),pNameList, pPrecioList,pCantidadList,pProcedenciaList,pFechaList,pTipoList,pImageList,pKeyList);
                mProductList.setAdapter(searchAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onStart() {
        super.onStart();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Producto, TwoFragment.ProductoViewHolder>(

                Producto.class,
                R.layout.row_productos,
                TwoFragment.ProductoViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(TwoFragment.ProductoViewHolder viewHolder, final Producto model, int position) {

                final String producto_key = getRef(position).getKey();
                final String prodname= model.getProdname();
                final String precio = model.getPrecio();
                final String cantidad = model.getCantidad();
                final String fechaor = model.getFechaor();
                final String procedencia = model.getProcedencia();
                final String tipoprod = model.getTipoprod();
                final String imageprod = model.getImageprod();

               // final String producto_key = getRef(position).getKey();
                viewHolder.setProdname(model.getProdname());
                viewHolder.setTipoprod(model.getTipoprod());
                viewHolder.setPrecio(model.getPrecio());
                viewHolder.setProcedencia(model.getProcedencia());
                viewHolder.setImagenprod(getActivity().getApplicationContext(), model.getImageprod());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Toast.makeText(MainActivity.this, post_key, Toast.LENGTH_LONG).show();

                            Intent singleBlogIntent = new Intent(getActivity().getApplicationContext(), ProductoSingleActivity.class);
                            singleBlogIntent.putExtra("blog_id", producto_key);
                            startActivity(singleBlogIntent);



                        }
                });

                //Like

                viewHolder.setLikeBtn(producto_key);
//mostrar on CLikc

                //Like

                viewHolder.mLikeStar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mProcessLike = true;


                        mDatabaseLike.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                if(mProcessLike) {

                                    if (dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(producto_key)) {
                                        Toast.makeText(getActivity().getApplicationContext(), "QUITÓ PRODUCTO",Toast.LENGTH_LONG).show();
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).removeValue();
                                        mDatabaseLike.child(producto_key).child("prodname").removeValue();
                                        mDatabaseLike.child(producto_key).child("precio").removeValue();
                                        mDatabaseLike.child(producto_key).child("cantidad").removeValue();
                                        mDatabaseLike.child(producto_key).child("fechaor").removeValue();
                                        mDatabaseLike.child(producto_key).child("procedencia").removeValue();
                                        mDatabaseLike.child(producto_key).child("tipoprod").removeValue();
                                        mDatabaseLike.child(producto_key).child("imageprod").removeValue();
                                        mProcessLike = false;


                                        mProductList.removeAllViews();

                                    } else {

                                       // Toast.makeText(getActivity().getApplicationContext(),producto_key1,Toast.LENGTH_LONG).show();

                                        Toast.makeText(getActivity().getApplicationContext(),"AGREGÓ PRODUCTO",Toast.LENGTH_LONG).show();
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).setValue("RandomValue");
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("prodname").setValue(prodname);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("precio").setValue(precio);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("cantidad").setValue(cantidad);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("fechaor").setValue(fechaor);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("procedencia").setValue(procedencia);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("tipoprod").setValue(tipoprod);
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).child("imageprod").setValue(imageprod);




                                        mProcessLike = false;

                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }


                });

            }
        };

        //ORDENADA
        // Here you modify your LinearLayoutManager
        mItemsLayoutManager = new GridLayoutManager(getContext(),2);
        //mItemsLayoutManager.setReverseLayout(true);
        //mItemsLayoutManager.setStackFromEnd(true);

        // Now set the layout manager and the adapter to the RecyclerView
        mProductList.setLayoutManager(mItemsLayoutManager);
//ORDENADA CIERRE

        mProductList.setAdapter(firebaseRecyclerAdapter);
        search_edit_text.setText(null);

    }


    public void busqueda() {
        super.onStart();

        FirebaseRecyclerAdapter<Producto, TwoFragment.ProductoViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Producto, TwoFragment.ProductoViewHolder>(

                Producto.class,
                R.layout.row_productos,
                TwoFragment.ProductoViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(TwoFragment.ProductoViewHolder viewHolder, Producto model, int position) {

                final String producto_key = getRef(position).getKey();

                // final String producto_key = getRef(position).getKey();
                viewHolder.setProdname(model.getProdname());
                viewHolder.setTipoprod(model.getTipoprod());
                viewHolder.setPrecio(model.getPrecio());
                viewHolder.setProcedencia(model.getProcedencia());
                viewHolder.setImagenprod(getActivity().getApplicationContext(), model.getImageprod());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Toast.makeText(MainActivity.this, post_key, Toast.LENGTH_LONG).show();

                        Intent singleBlogIntent = new Intent(getActivity().getApplicationContext(), ProductoSingleActivity.class);
                        singleBlogIntent.putExtra("prod_id", producto_key);
                        startActivity(singleBlogIntent);


                    }
                });

            }
        };

        mProductList.setAdapter(firebaseRecyclerAdapter);
        mProductList.getRecycledViewPool().clear();
       // searchAdapter.notifyDataSetChanged();

    }

    private  void checkUserExist(){

        if(mAuth.getCurrentUser()!= null){
            final String user_id = mAuth.getCurrentUser().getUid();

            mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if(!dataSnapshot.hasChild(user_id)){

                        Intent mainIntent = new Intent(getActivity().getApplicationContext(),getClass());
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);
                       // finish();

                    }

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }}


    public static class ProductoViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public ProductoViewHolder(View itemView) {
            super(itemView);

            mView=itemView;

            //Para el like
            mLikeStar=(ImageButton) mView.findViewById(R.id.imageButton);

            mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");
            mAuth =FirebaseAuth.getInstance();

        }

        ImageButton mLikeStar;
        DatabaseReference mDatabaseLike;
        FirebaseAuth mAuth;


        public  void  setProdname(String prodname){
            TextView post_prodname = (TextView) mView.findViewById(R.id.pName);
            post_prodname.setText(prodname);
        }

        public void setTipoprod(String tipoprod){
            TextView post_prodtipo = (TextView) mView.findViewById(R.id.pTipo);
            post_prodtipo.setText(tipoprod);

        }



        public void setPrecio(String precio){
            TextView post_precio = (TextView) mView.findViewById(R.id.pPrecio);
            post_precio.setText(precio);

        }



        public void setProcedencia(String procedencia){
            TextView post_procedencia = (TextView) mView.findViewById(R.id.pProcedencia);
            post_procedencia.setText(procedencia);

        }

        public void setImagenprod(Context ctx, String imageprod){

            ImageView post_imageprod = (ImageView)mView.findViewById(R.id.imgProduct);
            Picasso.with(ctx).load(imageprod).into(post_imageprod);


        }


        public void setLikeBtn(final String producto_key){

            if(mAuth.getCurrentUser()!= null){

                mDatabaseLike.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(producto_key)){


                            mLikeStar.setImageResource(R.drawable.estrellaamarilla);

                        }

                        else {

                            mLikeStar.setImageResource(R.drawable.estrellaverde);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }}


    }

}
