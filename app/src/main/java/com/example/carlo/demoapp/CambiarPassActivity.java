package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

public class CambiarPassActivity extends AppCompatActivity {

    public boolean permisoUppass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cambiar_pass);



    }

    public void GuardarPass(final Context context, final String pass, final String passconfirm, EditText textpass, EditText textpassconfirm){

        final ProgressDialog mProgress=new ProgressDialog(context);

        FirebaseUser user= FirebaseAuth.getInstance().getCurrentUser();


        validarPass(context,pass,passconfirm,textpass,textpassconfirm);

        if (permisoUppass==true)
        {
            mProgress.setMessage("Guardando Contraseña");
            mProgress.show();
            user.updatePassword(pass)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            mProgress.dismiss();
                            if (task.isSuccessful()){
                                Toast.makeText(context,"Contraseña Actuailzada",Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }

    }

    public void validarPass(Context context,String pass, String passconfirm, EditText textpass,EditText textpassconfirm){

        if (pass.isEmpty() || pass.length()<=5){
            textpass.setText(null);
            Toast.makeText(context,"Al menos 6 caracteres",Toast.LENGTH_SHORT).show();
            permisoUppass=false;
        }else {
            permisoUppass=true;
        }
        if (passconfirm.isEmpty() || pass.length()<=5 || !passconfirm.equals(pass)){
            textpassconfirm.setText(null);
            Toast.makeText(context,"Las contraseñas no coinciden",Toast.LENGTH_SHORT).show();
            permisoUppass=false;
        }else {
            permisoUppass=true;
        }
    }
}
