package com.example.carlo.demoapp;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by tapiaalvarez on 5/01/18.
 */

public class PerfilSingleActivity extends AppCompatActivity {

    private String mPost_Key =null;

    private DatabaseReference mDatabase;

    private TextView mUserSingleName;
    private ImageView mUserSingleImage;
    private TextView mUserSingleDatos;
    private TextView mUserSingleRUC;
    private TextView mUserSingleTelefono;
    private TextView mUserSingleCorreo;
    private TextView mUserSingleDireccion;


    private FirebaseAuth mAuth;

    private String direccion;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_singleperfil);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");

        mAuth=FirebaseAuth.getInstance();

        // Toast.makeText(BlogSingleActivity.this , post_key, Toast.LENGTH_LONG).show();

        mPost_Key=getIntent().getExtras().getString("blog_id");

        mUserSingleName = (TextView) findViewById(R.id.userNombre);
        mUserSingleImage=(ImageView) findViewById(R.id.imgProfile);
        mUserSingleDatos=(TextView) findViewById(R.id.userDatos);
        mUserSingleRUC=(TextView) findViewById(R.id.userRUC);
        mUserSingleTelefono=(TextView) findViewById(R.id.userTelefono);
        mUserSingleCorreo=(TextView) findViewById(R.id.userCorreo);
        mUserSingleDireccion=(TextView) findViewById(R.id.userDireccion);

        mDatabase.child(mPost_Key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String username_val=(String) dataSnapshot.child("name").getValue();
                String userdatos_val=(String) dataSnapshot.child("nameempresa").getValue();
                String userRUC_val=(String) dataSnapshot.child("ruc").getValue();
                String usertelefono_val=(String) dataSnapshot.child("telefono").getValue();
                String usercorreo_val=(String) dataSnapshot.child("email").getValue();

                Double latitud_val=Double.parseDouble(dataSnapshot.child("latitud").getValue().toString()) ;
                Double longitud_val=Double.parseDouble(dataSnapshot.child("longitud").getValue().toString()) ;

                LatLng lugar = new LatLng(latitud_val,longitud_val);
                obtenerDireccion(lugar);

                String userimage_val = (String) dataSnapshot.child("image").getValue();

                String post_uid= (String) dataSnapshot.child("uid").getValue();

                mUserSingleName.setText(username_val);
                mUserSingleDatos.setText(userdatos_val);
                mUserSingleRUC.setText(userRUC_val);
                mUserSingleTelefono.setText(usertelefono_val);
                mUserSingleCorreo.setText(usercorreo_val);
                mUserSingleDireccion.setText(direccion);


                Picasso.with(PerfilSingleActivity.this).load(userimage_val).into(mUserSingleImage);

                // Toast.makeText(BlogSingleActivity.this , mAuth.getCurrentUser().getUid(),Toast.LENGTH_LONG).show();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void obtenerDireccion(LatLng lugar){

        if (lugar.latitude != 0.0 && lugar.longitude != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(lugar.latitude, lugar.longitude, 1);
                if (!list.isEmpty()) {
                    Address address = list.get(0);
                    direccion = address.getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}
