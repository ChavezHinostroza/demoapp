package com.example.carlo.demoapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProductosByUserActivity extends AppCompatActivity {

    private RecyclerView mProductList;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseBusqueda;
    private DatabaseReference mDatabaseLike;
    private DatabaseReference mDatabaseUsers;
    private FirebaseAuth mAuth;

    private GridLayoutManager mItemsLayoutManager;

    FirebaseRecyclerAdapter<Producto, ProductoViewHolder> firebaseRecyclerAdapter;
    private ArrayList<String> pNameList;
    private ArrayList<String> pPrecioList;
    private ArrayList<String> pProcedenciaList;
    private ArrayList<String> pFechaList;
    private ArrayList<String> pCantidadList;
    private ArrayList<String> pTipoList;
    private ArrayList<String> pImageList;
    private ArrayList<String> pKeyList;

    private SearchAdapter searchAdapter;

    FirebaseUser firebaseUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productos_by_user);

    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
        mDatabaseBusqueda = FirebaseDatabase.getInstance().getReference();
        mDatabaseUsers = FirebaseDatabase.getInstance().getReference().child("users2");

        mProductList= (RecyclerView) findViewById(R.id.productlist2);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new GridLayoutManager(getApplicationContext(),2));

        String userKey = firebaseUser.getUid();

        mProductList= (RecyclerView) findViewById(R.id.productlist2);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new GridLayoutManager(getBaseContext(),2));

        pNameList= new ArrayList<>();
        pPrecioList= new ArrayList<>();
        pProcedenciaList= new ArrayList<>();
        pFechaList= new ArrayList<>();
        pCantidadList= new ArrayList<>();
        pTipoList= new ArrayList<>();
        pImageList= new ArrayList<>();
        pKeyList=new ArrayList<>();

        checkUserExist();

        setAdapter(userKey);

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Producto, ProductosByUserActivity.ProductoViewHolder>(
                Producto.class,
                R.layout.row_productos,
                ProductosByUserActivity.ProductoViewHolder.class,
                mDatabase
        ) {
            @Override
            protected void populateViewHolder(ProductosByUserActivity.ProductoViewHolder viewHolder, Producto model, int position) {

                viewHolder.setProdname(model.getProdname());
                viewHolder.setTipoprod(model.getTipoprod());
                viewHolder.setPrecio(model.getPrecio());
                viewHolder.setProcedencia(model.getProcedencia());
                viewHolder.setImagenprod(getBaseContext(), model.getImageprod());

            }
        };

        mItemsLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        mProductList.setLayoutManager(mItemsLayoutManager);
        mProductList.setAdapter(firebaseRecyclerAdapter);
    }

    private  void checkUserExist(){

        if(mAuth.getCurrentUser()!= null){
            final String user_id = mAuth.getCurrentUser().getUid();

            mDatabaseUsers.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    if(!dataSnapshot.hasChild(user_id)){

                        Intent mainIntent = new Intent(getApplicationContext().getApplicationContext(),getClass());
                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(mainIntent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }}


        private void setAdapter(final String foranea){

            mDatabaseBusqueda.child("Producto").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {

                    pNameList.clear();
                    pPrecioList.clear();
                    pCantidadList.clear();
                    pProcedenciaList.clear();
                    pFechaList.clear();
                    pTipoList.clear();
                    pImageList.clear();
                    pKeyList.clear();
                    mProductList.removeAllViews();

                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        //String uid = snapshot.getKey();
                        String uid = snapshot.child("uid").getValue(String.class);
                        String prodname = snapshot.child("prodname").getValue(String.class);
                        String precio = snapshot.child("precio").getValue(String.class);
                        String cantidad = snapshot.child("cantidad").getValue(String.class);
                        String fechaor = snapshot.child("fechaor").getValue(String.class);
                        String procedencia = snapshot.child("procedencia").getValue(String.class);
                        String tipoprod = snapshot.child("tipoprod").getValue(String.class);
                        String imageprod = snapshot.child("imageprod").getValue(String.class);
                        String keyprod = snapshot.getKey();


                        if (uid.toLowerCase().contains(foranea.toLowerCase())) {
                            pNameList.add(prodname);
                            pPrecioList.add(precio);
                            pCantidadList.add(cantidad);
                            pProcedenciaList.add(procedencia);
                            pFechaList.add(fechaor);
                            pTipoList.add(tipoprod);
                            pImageList.add(imageprod);
                            pKeyList.add(keyprod);

                        }

                    }

                    searchAdapter = new SearchAdapter(getBaseContext(),pNameList, pPrecioList,pCantidadList,pProcedenciaList,pFechaList,pTipoList,pImageList,pKeyList);
                    mProductList.setAdapter(searchAdapter);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }


        public static class ProductoViewHolder extends RecyclerView.ViewHolder{

            View mView;

            public ProductoViewHolder(View itemView){
                super(itemView);

                mView=itemView;
                mAuth =FirebaseAuth.getInstance().getCurrentUser();

            }

            FirebaseUser mAuth;

            public  void  setProdname(String prodname){
                TextView post_prodname = (TextView) mView.findViewById(R.id.pName);
                post_prodname.setText(prodname);
            }

            public void setTipoprod(String tipoprod){
                TextView post_prodtipo = (TextView) mView.findViewById(R.id.pTipo);
                post_prodtipo.setText(tipoprod);

            }


            public void setPrecio(String precio){
                TextView post_precio = (TextView) mView.findViewById(R.id.pPrecio);
                post_precio.setText(precio);

            }


            public void setProcedencia(String procedencia){
                TextView post_procedencia = (TextView) mView.findViewById(R.id.pProcedencia);
                post_procedencia.setText(procedencia);

            }

            public void setImagenprod(Context ctx, String imageprod){

                ImageView post_imageprod = (ImageView)mView.findViewById(R.id.imgProduct);
                Picasso.with(ctx).load(imageprod).into(post_imageprod);


            }

        }

}
