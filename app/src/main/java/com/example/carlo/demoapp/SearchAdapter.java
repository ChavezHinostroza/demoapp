package com.example.carlo.demoapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by tapiaalvarez on 22/12/17.
 */

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.SearchViewHolder> {
    Context context;
    ArrayList<String> pNameList;
    ArrayList<String> pPrecioList;
    ArrayList<String> pProcedenciaList;
    ArrayList<String> pTipoList;
    ArrayList<String> pImageList;
    ArrayList<String> pKeyList;
    FirebaseRecyclerAdapter<Producto, TwoFragment.ProductoViewHolder> firebaseRecyclerAdapter;


    class SearchViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView pName, pPrecio, pCantidad, pProcedencia, pFecha, pTipo;

        public SearchViewHolder(View itemView) {
            super(itemView);
            imgProduct = (ImageView) itemView.findViewById(R.id.imgProduct);
            pName = (TextView) itemView.findViewById(R.id.pName);
            pPrecio = (TextView) itemView.findViewById(R.id.pPrecio);
            pCantidad = (TextView) itemView.findViewById(R.id.pCantidad);
            pProcedencia = (TextView) itemView.findViewById(R.id.pProcedencia);
            pFecha = (TextView) itemView.findViewById(R.id.pFecha);
            pTipo = (TextView) itemView.findViewById(R.id.pTipo);

        }
    }


    public SearchAdapter(Context context, ArrayList<String> pNameList, ArrayList<String> pPrecioList,
                         ArrayList<String> pCantidadList,ArrayList<String> pProcedenciaList, ArrayList<String> pFechaList,
                         ArrayList<String> pTipoList, ArrayList<String> pImageList, ArrayList<String> pKeyList) {
        this.context = context;
        this.pNameList = pNameList;
        this.pPrecioList = pPrecioList;
        this.pProcedenciaList = pProcedenciaList;
        this.pTipoList = pTipoList;
        this.pImageList= pImageList;
        this.pKeyList=pKeyList;


    }

    @Override
    public SearchAdapter.SearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_productos, parent, false);
        return new SearchAdapter.SearchViewHolder(view);
    }


    @Override
    public void onBindViewHolder(final SearchViewHolder holder,final int position) {
        holder.pName.setText(pNameList.get(position));
        holder.pPrecio.setText(pPrecioList.get(position));
        holder.pProcedencia.setText(pProcedenciaList.get(position));
        holder.pTipo.setText(pTipoList.get(position));
        String key=pKeyList.get(position);

        //COLOR
        holder.pName.setTextColor(Color.GRAY);
        holder.pTipo.setTextColor(Color.GRAY);
        holder.pPrecio.setTextColor(Color.GRAY);
        holder.pProcedencia.setTextColor(Color.GRAY);


        Glide.with(context).load(pImageList.get(position)).asBitmap().placeholder(R.drawable.fruits1).into(holder.imgProduct);

        holder.imgProduct.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("Producto");

                //Log.i("MARQUER_TAG",String.valueOf(key));
                ref.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        //for (DataSnapshot objSnapshot: snapshot.getChildren()) {

                            String obj=pKeyList.get(position);
                            //Toast.makeText(context, obj, Toast.LENGTH_LONG).show();
                            Intent intent = new Intent(context,ProductoSingleActivity.class);
                            intent.putExtra("blog_id",obj);

                            //Log.i("MARQUER_TAG",String.valueOf(obj));

                            context.startActivity(intent);


                        //}
                    }
                    @Override
                    public void onCancelled(DatabaseError firebaseError) {
                        Log.e("Read failed", firebaseError.getMessage());
                    }
                });


            }
        });
    }


    @Override
    public int getItemCount() {
        return pNameList.size();
    }

}
