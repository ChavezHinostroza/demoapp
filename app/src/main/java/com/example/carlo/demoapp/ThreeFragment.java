package com.example.carlo.demoapp;

import android.*;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.text.Layout;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;


public class ThreeFragment extends Fragment {

    public ImageButton btnGoMapa;
    String mensaje;

    public ThreeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_three, container, false);

    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);


        // get the button view
        btnGoMapa = (ImageButton) getView().findViewById(R.id.btnSMS);

        //verificar permisos
        if(ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.SEND_SMS)!= PackageManager.PERMISSION_GRANTED&& ActivityCompat.checkSelfPermission(getActivity(),Manifest
                .permission.SEND_SMS)!=PackageManager.PERMISSION_GRANTED){ActivityCompat.requestPermissions(getActivity(),new String[]
                { Manifest.permission.SEND_SMS,},1000);
        }else{

        }
        // set a onclick listener for when the button gets clicked
        btnGoMapa.setOnClickListener(new View.OnClickListener() {
            // Start new list activity
            public void onClick(View v) {
               //enviarSMS(v);
                Intent mainIntent = new Intent(getActivity(), MapsActivity.class);
                startActivity(mainIntent);
            }
        });

    }

    public void enviarSMS(View view) {

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage("numero phono", null, " mensaje aaaaa " + mensaje, null, null);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
