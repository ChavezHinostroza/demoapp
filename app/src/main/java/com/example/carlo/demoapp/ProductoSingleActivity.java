package com.example.carlo.demoapp;

import android.*;
import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.telephony.SmsManager;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


/**
 * Created by tapiaalvarez on 2/01/18.
 */

public class ProductoSingleActivity extends AppCompatActivity{

    private String mPost_Key =null;

    private DatabaseReference mDatabase;

        private TextView mProdSingleName;
        private ImageView mProdSingleImage;
        private TextView mProdSingleTipo;
        private TextView mProdSingleCantidad;
        private TextView mProdSinglePrecio;
        private TextView mProdSingleFechaOr;
        private TextView mProdSingleProcedencia;
        private String mServSingleTelefono;
        private String productKey;
        private String emailToSend;
        private String nameProducto;

    private FirebaseAuth mAuth;

     private Button btnEliminarProducto;
     private Button mSingleSMSBtn;
     private Button btnEditarProducto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_product2);

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");

        mAuth=FirebaseAuth.getInstance();

        // Toast.makeText(BlogSingleActivity.this , post_key, Toast.LENGTH_LONG).show();

        mPost_Key=getIntent().getExtras().getString("blog_id");

        mProdSingleName = (TextView) findViewById(R.id.prodName);
        mProdSingleImage=(ImageView) findViewById(R.id.imgProducto);
        mProdSingleTipo=(TextView) findViewById(R.id.prodTipo);
        mProdSingleCantidad=(TextView) findViewById(R.id.prodCantidad);
        mProdSingleFechaOr=(TextView) findViewById(R.id.prodFecha);
        mProdSingleProcedencia=(TextView) findViewById(R.id.prodProcedencia);
        mProdSinglePrecio=(TextView) findViewById(R.id.prodPrecio);

        btnEliminarProducto=(Button) findViewById(R.id.btnDelete);
        mSingleSMSBtn=(Button) findViewById(R.id.btnSMS);
        btnEditarProducto=(Button)findViewById(R.id.btnEdit);

        mDatabase.child(mPost_Key).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                productKey=dataSnapshot.getKey();
                nameProducto=(String) dataSnapshot.child("prodname").getValue();
                String stipoprod_val=(String) dataSnapshot.child("tipoprod").getValue();
                String scantidad_val=(String) dataSnapshot.child("cantidad").getValue();
                String sprecio_val=(String) dataSnapshot.child("precio").getValue();
                String sprocedencia_val=(String) dataSnapshot.child("procedencia").getValue();
                String scropdate_val=(String) dataSnapshot.child("fechaor").getValue();
                String simage_val = (String) dataSnapshot.child("imageprod").getValue();

                String stelefono_val = (String) dataSnapshot.child("telefono").getValue();
                emailToSend = (String) dataSnapshot.child("email").getValue();

                String post_uid= (String) dataSnapshot.child("uid").getValue();

                mProdSingleName.setText(nameProducto);
                mProdSingleTipo.setText(stipoprod_val);
                mProdSingleCantidad.setText(scantidad_val);
                mProdSinglePrecio.setText(sprecio_val);
                mProdSingleProcedencia.setText(sprocedencia_val);
                mProdSingleFechaOr.setText(scropdate_val);

                mServSingleTelefono=stelefono_val;



                Picasso.with(ProductoSingleActivity.this).load(simage_val).into(mProdSingleImage);

                // Toast.makeText(BlogSingleActivity.this , mAuth.getCurrentUser().getUid(),Toast.LENGTH_LONG).show();

                if(mAuth.getCurrentUser().getUid().equals(post_uid)){

                    //Toast.makeText(ProductoSingleActivity.this , post_uid,Toast.LENGTH_LONG).show();

                    btnEliminarProducto.setVisibility(View.VISIBLE);
                    btnEditarProducto.setVisibility(View.VISIBLE);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnEliminarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DeleteProducto();

            }
        });

        assert  mSingleSMSBtn !=null;
        mSingleSMSBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Mensajerisirijillo();

            }
        });

        btnEditarProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mainIntent = new Intent(ProductoSingleActivity.this, EditProductActivity.class);
                mainIntent.putExtra("prodKey",productKey);
                startActivity(mainIntent);
            }
        });

    }

    private void DeleteProducto(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("¿Desea eliminar este Producto: "+nameProducto+" ?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mDatabase.child(mPost_Key).removeValue();
                    Intent mainIntent = new Intent(ProductoSingleActivity.this, Main2Activity.class);
                    startActivity(mainIntent);


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "Error al eliminar, intente de nuevo!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();

    }


    public void Mensajerisirijillo(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("¿Desea enviar un aviso?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    SmsManager smsManager = SmsManager.getDefault();
                    smsManager.sendTextMessage(mServSingleTelefono, null, "Este número está interesado en contactarte", null, null);

                    MensajeCorreo();


                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(),
                            "Mensaje fallo, intenta después!",
                            Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if user select "No", just cancel this dialog and continue with app
                dialog.cancel();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public void MensajeCorreo(){

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        String[] para={emailToSend};
        String[] CC = {""};

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, para);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);

        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Información Acerca de "+nameProducto);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Esta persona desea comunicarse contigo");

        try {
            startActivity(Intent.createChooser(emailIntent, "Enviar email..."));

            finish();
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "email enviado, espere respuesta",
                    Toast.LENGTH_LONG).show();
        }

    }

}


