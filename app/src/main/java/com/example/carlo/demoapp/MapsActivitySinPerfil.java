package com.example.carlo.demoapp;

import android.*;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivitySinPerfil extends FragmentActivity implements OnMapReadyCallback, LocationListener {

    private GoogleMap mMap;

    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private DatabaseReference mDatabase;
    private LocationManager locationManager1;

    public double latitud;
    public double longitud;
    public String direccion;
    public String direccionBD;
    public double latitudBD;
    public double longitudBD;

    public boolean activadormarker;

    public Button guardar;

    public View popup=null;

    public TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_sin_perfil);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        Toast toast=Toast.makeText(getBaseContext(), "Mantenga presionado el lugar a guardar", Toast.LENGTH_LONG);
        toast.setGravity(Gravity.BOTTOM,0,0);
        toast.show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng lima = new LatLng(-12.045925, -77.030546);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lima));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                if (popup==null){
                    popup=getLayoutInflater().inflate(R.layout.popup_up_marker,null);
                }
                tv=(TextView)popup.findViewById(R.id.title);
                tv.setText(marker.getTitle());

                tv=(TextView)popup.findViewById(R.id.snippet);
                tv.setText(marker.getSnippet());


                return popup;
            }
        });

        guardarLugar();
        marcarEnMapa();
        jalarDatos();
    }

    public void guardarLugar(){
        guardar = (Button)findViewById(R.id.btnGetLugar);
        guardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentPerfil = new Intent(MapsActivitySinPerfil.this, UserProfileStyleActivity.class);
                intentPerfil.putExtra("LATITUD",latitud);
                intentPerfil.putExtra("LONGITUD",longitud);
                startActivity(intentPerfil);
                Toast toast=Toast.makeText(getBaseContext(), "Finalice el proceso dando a 'Guardar Cambios'", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.TOP,0,0);
                toast.show();
            }
        });

    }

    public void marcarEnMapa(){
        activadormarker=true;
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {

                if (activadormarker==true){

                    latitud=latLng.latitude;
                    longitud=latLng.longitude;
                    activadormarker=false;
                    obtenerDireccion();
                    MarkerOptions marker= new MarkerOptions().position(latLng).title("...").snippet(direccion);
                    mMap.addMarker(marker);
                    Toast toast=Toast.makeText(getBaseContext(), "Ahora guarde la ubicación", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.TOP,0,0);
                    toast.show();
                }
            }
        });
    }

    public void  marcarEnMapaBD(LatLng lugarBD, String nombreEmpresa, String telefono){

        obtenerDireccionBD(lugarBD);

        MarkerOptions marker= new MarkerOptions().position(lugarBD).title(nombreEmpresa+" - "+telefono)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icono30px)).snippet(direccionBD);
        if (lugarBD.latitude != 0.0 && lugarBD.longitude != 0.0)
        mMap.addMarker(marker);

    }

    public void obtenerDireccion(){

        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        List<Address> list = null;
        try {
            list = geocoder.getFromLocation(latitud, longitud, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Address address = list.get(0);
        direccion = address.getAddressLine(0);

    }

    public void obtenerDireccionBD(LatLng lugarBD){

        if (lugarBD.latitude != 0.0 && lugarBD.longitude != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(lugarBD.latitude, lugarBD.longitude, 1);
                if (!list.isEmpty()) {
                    Address address = list.get(0);
                    direccionBD = address.getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public void jalarDatos(){

        mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Double.parseDouble(dataSnapshot.child("latitud").getValue().toString()) ;
                String nombreEmpresa=dataSnapshot.child("nameempresa").getValue().toString();
                latitudBD=Double.parseDouble(dataSnapshot.child("latitud").getValue().toString()) ;
                longitudBD=Double.parseDouble(dataSnapshot.child("longitud").getValue().toString()) ;
                String telefono=dataSnapshot.child("telefono").getValue().toString();

                LatLng lugarBD = new LatLng(latitudBD,longitudBD);

                marcarEnMapaBD(lugarBD, nombreEmpresa, telefono);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
