package com.example.carlo.demoapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by tapiaalvarez on 20/12/17.
 */

public class ProductActivity extends AppCompatActivity {

    private RecyclerView mProductList;
    private LinearLayoutManager mProdLayoutManager;
    private DatabaseReference mDatabase,mDatabaseLike;
    Toolbar toolbar;
    private FirebaseAuth mAuth;
    FirebaseRecyclerAdapter<Producto, ProductActivity.ProductoViewHolder> firebaseRecyclerAdapter;
    private FirebaseAuth.AuthStateListener mAuthListener;
    //FirebaseUser firebaseUser;
    private FirebaseUser mCurrentUser;
    private DatabaseReference mDatabaseUsers;

    private String mPost_Key =null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        toolbar = (Toolbar) findViewById(R.id.toolbaradd);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.left_arrow); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });



        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent Loginintent = new Intent(ProductActivity.this,ActivityRegister2.class);
                    Loginintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(Loginintent);

                }
            }
        };

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
        mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");
       // mDatabaseb = FirebaseDatabase.getInstance().getReference().child("Producto");
       mDatabaseUsers=FirebaseDatabase.getInstance().getReference().child("Producto").child(mCurrentUser.getUid());

        mProductList= (RecyclerView) findViewById(R.id.productlist);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new LinearLayoutManager(this));
       // mProductList.setLayoutManager(new GridLayoutManager(this, 2));








    }


    @Override
    protected void onStart() {
        super.onStart();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Producto, ProductActivity.ProductoViewHolder>(

                Producto.class,
                R.layout.row_productos,
                ProductActivity.ProductoViewHolder.class,
                mDatabase

        ) {
            @Override
            protected void populateViewHolder(final ProductActivity.ProductoViewHolder viewHolder,final Producto model, int position) {

                final String user_id = mAuth.getCurrentUser().getUid();
                final String producto_key = getRef(position).getKey();

                mDatabaseUsers.child("prodname").equalTo("Cifrut").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.child(producto_key).hasChild(user_id)){

                        viewHolder.setProdname(model.getProdname());
                        viewHolder.setTipoprod(model.getTipoprod());
                        viewHolder.setPrecio(model.getPrecio());
                        viewHolder.setFechaor(model.getFechaor());
                        viewHolder.setProcedencia(model.getProcedencia());

                        viewHolder.setImagenprod(getApplicationContext(), model.getImageprod());

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });



             /*   mDatabase.child("Producto").orderByChild("name").equalTo("Cifrut").addChildEventListener(new ChildEventListener() {


                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {*//*
                        if (names.equals(post_uid)) {*//*

                        viewHolder.setProdname(model.getProdname());
                        viewHolder.setTipoprod(model.getTipoprod());
                        viewHolder.setCantidad(model.getCantidad());
                        viewHolder.setPrecio(model.getPrecio());
                        viewHolder.setFechaor(model.getFechaor());
                        viewHolder.setProcedencia(model.getProcedencia());
                        viewHolder.setImagenprod(getApplicationContext(), model.getImageprod());

                       }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });*/

               /* final String user_id = mAuth.getCurrentUser().getUid();

                if(mAuth.getCurrentUser().getUid().equals(user_id)) {
                    final String producto_key = getRef(position).getKey();
                }*/
            }
        };

        mProductList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class ProductoViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public ProductoViewHolder(View itemView) {
            super(itemView);

            mView=itemView;

        }


        public  void  setProdname(String prodname){
            TextView post_prodname = (TextView) mView.findViewById(R.id.pName);
            post_prodname.setText(prodname);
        }

        public void setTipoprod(String tipoprod){
            TextView post_prodtipo = (TextView) mView.findViewById(R.id.pTipo);
            post_prodtipo.setText(tipoprod);

        }



        public void setPrecio(String precio){
            TextView post_precio = (TextView) mView.findViewById(R.id.pPrecio);
            post_precio.setText(precio);

        }

        public void setFechaor(String fechaor){
            TextView post_fechaor = (TextView) mView.findViewById(R.id.pFecha);
            post_fechaor.setText(fechaor);

        }

        public void setProcedencia(String procedencia){
            TextView post_procedencia = (TextView) mView.findViewById(R.id.pProcedencia);
            post_procedencia.setText(procedencia);

        }

        public void setImagenprod(Context ctx, String imageprod){

            ImageView post_imageprod = (ImageView)mView.findViewById(R.id.imgProduct);
            Picasso.with(ctx).load(imageprod).into(post_imageprod);


        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main2, menu);


        return super.onCreateOptionsMenu(menu);
    }



}
