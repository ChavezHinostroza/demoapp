package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ActivityLoginEmailPass extends AppCompatActivity {

    private EditText textEmail;
    private EditText textPass;
    private Button btnRegister;
    private ProgressDialog progressDialog;
    private FirebaseAuth mAuth;
    DatabaseReference mpasscode1;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private CardView cvbtn1,cvbtn2;
    private TextView txtolvidepass;

    Button btnSgt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_email_pass);
        //para que no sea horizontal
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        getSupportActionBar().hide();

        progressDialog = new ProgressDialog(this);
        textEmail = (EditText) findViewById(R.id.etxt_email);

        txtolvidepass =(TextView)findViewById(R.id.tvolvPass);

        textPass = (EditText) findViewById(R.id.etxt_password);
        btnRegister = (Button) findViewById(R.id.btn_login);
        mAuth = FirebaseAuth.getInstance();


        cvbtn1 = (CardView)findViewById(R.id.cdbtn1);
        cvbtn1.getBackground().setAlpha(168);

        cvbtn2 = (CardView)findViewById(R.id.cdbtn2);
        cvbtn2.getBackground().setAlpha(168);

        btnRegister.getBackground().setAlpha(168);

        txtolvidepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentpass = new Intent(ActivityLoginEmailPass.this, ResetPassActivity.class);
                startActivity(intentpass);
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doLogin();
            }
        });

       // mpasscode1= FirebaseDatabase.getInstance().getReference().child("users");
       // final String passc = dataSnapshot.child("passcode").getValue().toString();



        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull final FirebaseAuth firebaseAuth) {

                if (firebaseAuth.getCurrentUser() != null) {
                   /*
                    //String namees = textEmail.getText().toString();


                    //Intent intent = new Intent(ActivityLoginEmailPass.this, ActivityLoginEmailPass.class);
                    //startActivity(intent);
                    //finish():



                    //mAuth.signOut();

                    //caso 3

                    Intent intent = new Intent(ActivityLoginEmailPass.this, Main2Activity.class);
                    startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT));


                    //caso 4

                    Intent intent= getIntent();
                    finish();
                    startActivity(intent);
                    */

                }

                FirebaseUser user = firebaseAuth.getCurrentUser();
                if(user != null && user.isEmailVerified()){
                   // Toast.makeText(ActivityLoginEmailPass.this, "Bienvenido  "  , Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(ActivityLoginEmailPass.this, Main2Activity.class);
                    startActivity(intent);
                }
            }
        };




        //IR AL DE REGISTRO
        btnSgt = (Button)findViewById(R.id.btnNuevoUsuario);
        btnSgt.getBackground().setAlpha(178);

        btnSgt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityLoginEmailPass.this,ActivityRegister2.class);
                startActivity(intent);
            }
        });


      /*  FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        setContentView(R.layout.activity_login_email_pass);

        if (getIntent().hasExtra("logout")) {
            LoginManager.getInstance().logOut();
        }
        mAuth = FirebaseAuth.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
                Log.d("", "facebook:onSuccess:" + loginResult);
            }

            @Override
            public void onCancel() {
                Log.d("TAG", "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d("TAG", "facebook:onError", error);
            }
        });

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                    Log.d("", "onAuthStateChanged:signed_in:" + user.getUid());

                    Intent intent = new Intent(ActivityLoginEmailPass.this, MainActivity.class);
                    String email = user.getEmail();
                    String name = user.getDisplayName();
                    startActivity(intent);
                    finish();
                } else {
                    Log.d("TG", "SIGNED OUT");
                }
            }
        };
*/

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    /* @Override
     public void onStop() {
         super.onStop();
         if (mAuthListener != null) {
             mAuth.removeAuthStateListener(mAuthListener);
         }
     }

     private void handleFacebookAccessToken(AccessToken token) {
         Log.d("", "handleFacebookAccessToken:" + token);

         AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
         mAuth.signInWithCredential(credential)
                 .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                     @Override
                     public void onComplete(@NonNull Task<AuthResult> task) {
                         Log.d("", "signInWithCredential:onComplete:" + task.isSuccessful());
                         if (!task.isSuccessful()) {
                             Log.w("", "signInWithCredential", task.getException());
                             Toast.makeText(ActivityLoginEmailPass.this, "Autentificación fallida.",
                                     Toast.LENGTH_SHORT).show();
                         }

                     }
                 });
     }

     @Override
     public void onActivityResult(int requestCode, int resultCode, Intent data) {
         super.onActivityResult(requestCode, resultCode, data);
         mCallbackManager.onActivityResult(requestCode,
                resultCode, data);
     }

 */
    private void doLogin() {
        String email = textEmail.getText().toString().trim();
        String password = textPass.getText().toString().trim();

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {
            progressDialog.setMessage("Iniciando Sesión , por favor espere");
            progressDialog.show();
            mAuth.signInWithEmailAndPassword(email, password)
                    .addOnCompleteListener(ActivityLoginEmailPass.this,new OnCompleteListener<AuthResult>() {


                        //aca
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                         progressDialog.dismiss();
                         /*
                         Intent intent = new Intent(ActivityLoginEmailPass.this, Main2Activity.class);
                         startActivity(intent);
                         */

                               // FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                FirebaseUser user=mAuth.getCurrentUser();
                                try {
                                    if(user.isEmailVerified()){
                                        Intent intent = new Intent(ActivityLoginEmailPass.this, Main2Activity.class);
                                        startActivity(intent);
                                       // Toast.makeText(ActivityLoginEmailPass.this,"Email verificado", Toast.LENGTH_SHORT).show();
                                    }else {
                                        Toast.makeText(ActivityLoginEmailPass.this,"Email no está verificado", Toast.LENGTH_SHORT).show();
                                        //mAuth.signOut();
                                    }

                                }
                                catch(NullPointerException e){
                                    Toast.makeText(ActivityLoginEmailPass.this,"Datos incorrectos", Toast.LENGTH_SHORT).show();

                                }

                           /*
                            if (task.isSuccessful()) {

                                Toast.makeText(ActivityLoginEmailPass.this, "Inicio Sesión Satisfactorio", Toast.LENGTH_SHORT).show();
                            } else
                                Toast.makeText(ActivityLoginEmailPass.this,"Error al Iniciar Sesión", Toast.LENGTH_SHORT).show();
                                */
                        }
                    });
        } else {

            Toast.makeText(ActivityLoginEmailPass.this,"Faltan completar campos", Toast.LENGTH_SHORT).show();
        }
    }
}
