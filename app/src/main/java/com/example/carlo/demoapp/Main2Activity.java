package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private DatabaseReference mDatabase;
    FirebaseAuth.AuthStateListener mAuthListener;
    FirebaseAuth mAuth;
    private StorageReference mStorage;
    private TextView Nomre, correo;
    private ImageView imageProfile;
    //NavigationView navigation_view;
    FirebaseUser firebaseUser;
    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        //DATOS DE PERFIL PARA EL MENU


        mAuth = FirebaseAuth.getInstance();
        firebaseUser = mAuth.getCurrentUser();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    /*Intent Loginintent = new Intent(Main2Activity.this,ActivityRegister.class);
                    Loginintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(Loginintent);*/
                    NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                    Menu nav_Menu = navigationView.getMenu();
                    nav_Menu.findItem(R.id.nav_comprar).setVisible(true);
                    nav_Menu.findItem(R.id.nav_vender).setVisible(false);
                    nav_Menu.findItem(R.id.nav_misprod).setVisible(true);
                    //nav_Menu.findItem(R.id.nav_coleccion).setVisible(false);

                }
            }
        };




        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    mStorage = FirebaseStorage.getInstance().getReference();
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
                    mDatabase.child(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                          //  Nomre.setText(dataSnapshot.child("name").getValue().toString());
                          //  correo.setText(dataSnapshot.child("email").getValue().toString());


                           /* String imageUrl = dataSnapshot.child("image").getValue().toString();
                            if (!imageUrl.equals("default") || TextUtils.isEmpty(imageUrl))
                                Picasso.with(Main2Activity.this).load(Uri.parse(dataSnapshot.child("image")
                                        .getValue()
                                        .toString()))
                                        .into(imageProfile);*/

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    startActivity(new Intent(Main2Activity.this, ActivityLoginEmailPass.class));
                    finish();
                }
            }
        };

       /* FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);



        //navigationView.removeHeaderView(null);
        navigationView.removeHeaderView(navigationView.getHeaderView(0));
        View header = LayoutInflater.from(this).inflate(R.layout.nav_header_main2, null);
        navigationView.addHeaderView(header);
        final TextView Nomre = (TextView)header.findViewById(R.id.tvNameProfileMenu);
        final  TextView correo = (TextView)header.findViewById(R.id.tvCorreoMenu);
        imageProfile = (ImageView)header.findViewById(R.id.ivProfileMenu);


          /*  View avHeaderView = navigationView.inflateHeaderView(R.layout.nav_header_main2);
            final TextView Nomre = (TextView)avHeaderView.findViewById(R.id.tvNameProfileMenu);
            final  TextView correo = (TextView)avHeaderView.findViewById(R.id.tvCorreoMenu);
            imageProfile = (ImageView)avHeaderView.findViewById(R.id.ivProfileMenu);*/


           mAuth = FirebaseAuth.getInstance();
           mAuthListener = new FirebaseAuth.AuthStateListener() {
               @Override
               public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                   if (firebaseAuth.getCurrentUser() != null) {
                       mStorage = FirebaseStorage.getInstance().getReference();
                       mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
                       mDatabase.child(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                           @Override
                           public void onDataChange(DataSnapshot dataSnapshot) {
                               Nomre.setText(dataSnapshot.child("name").getValue().toString());
                               correo.setText(dataSnapshot.child("email").getValue().toString());

                               String post_uid = dataSnapshot.child("tipoUser").getValue().toString();
                               String name = "Comprador";
                               //Toast.makeText(Main2Activity.this , post_uid,Toast.LENGTH_LONG).show();
                               if(post_uid.equals(name)){

                                   NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                                   Menu nav_Menu = navigationView.getMenu();

                                   nav_Menu.findItem(R.id.nav_comprar).setVisible(false);
                                   nav_Menu.findItem(R.id.nav_misprod).setVisible(false);


                                   //mSingleRemoveBtn.setVisibility(View.VISIBLE);
                               }

                           String imageUrl = dataSnapshot.child("image").getValue().toString();
                            if (!imageUrl.equals("default") || TextUtils.isEmpty(imageUrl))
                                Picasso.with(Main2Activity.this).load(Uri.parse(dataSnapshot.child("image")
                                        .getValue()
                                        .toString()))
                                        .into(imageProfile);
                           }

                           @Override
                           public void onCancelled(DatabaseError databaseError) {

                           }
                       });
                   } else {
                       startActivity(new Intent(Main2Activity.this, ActivityLoginEmailPass.class));
                       finish();
                   }
               }
           };
    }

    //aca se cambia el orden de los fragmentos
    private void setupViewPager(ViewPager viewPager) {
        Main2Activity.ViewPagerAdapter adapter = new Main2Activity.ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new TwoFragment(), "PRODUCTOS");
        adapter.addFragment(new OneFragment(), "MIS FAVORITOS");

        adapter.addFragment(new ThreeFragment(), "MAPA");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //popup window code here
            AlertDialog alertDialog = new AlertDialog.Builder(Main2Activity.this).create();
            alertDialog.setTitle("Demo App");
            alertDialog.setMessage("App desarrollada por Swat Team " + '\n'+'\n' +"Versión 0.1: " +
                    '\n' +" © Todos los derechos reservados   ");

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
            alertDialog.show();
            return true;
        }
        if(id == R.id.action_logout){
            if (mAuth.getCurrentUser() != null)
                mAuth.signOut();
            return true;
        }
        /*if(id == R.id.action_profile){
             Intent GoToPerfilActivity = new Intent(Main2Activity.this, PerfilActivity.class);
                startActivity(GoToPerfilActivity);
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
           /* Intent intent = new Intent(Main2Activity.this,PerfilActivity.class);
            startActivity(intent);*/

            Intent intent =  new Intent(Main2Activity.this,UserProfileStyleActivity.class);
            startActivity(intent);


        } else if (id == R.id.nav_comprar) {
            Intent intent = new Intent(Main2Activity.this,PostProductActivity.class);
            startActivity(intent);


        }
        /*else if (id == R.id.nav_coleccion) {

            Intent intent = new Intent(Main2Activity.this,ProductActivity.class);
            startActivity(intent);
        }*/
    else if (id == R.id.nav_misprod) {
        Intent intent = new Intent(Main2Activity.this,ProductosByUserActivity.class);
        startActivity(intent);


    }

        else if (id == R.id.nav_vender) {

            /*Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);*/
            Intent intent = new Intent(Main2Activity.this,Main2Activity.class);
            startActivity(intent);



        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
