package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPassActivity extends AppCompatActivity {

    private EditText txtEmail;
    private Button btnReset;
    private ProgressDialog progressDialog;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_pass);

        txtEmail=(EditText)findViewById(R.id.txtEmailToReset);
        btnReset=(Button)findViewById(R.id.btnResetPass);

        btnReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReestablecerPass();
            }
        });
    }

    private void ReestablecerPass(){

        String correo=txtEmail.getText().toString();

        mAuth=FirebaseAuth.getInstance();
        mAuth.sendPasswordResetEmail(correo)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()){
                            Intent intent = new Intent(ResetPassActivity.this, ActivityLoginEmailPass.class);
                            startActivity(intent);
                            Toast.makeText(ResetPassActivity.this, "Nueva contraseña enviada al correo ingresado"  , Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
