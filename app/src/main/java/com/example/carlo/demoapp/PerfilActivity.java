package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.math.BigInteger;
import java.security.SecureRandom;

public class PerfilActivity extends AppCompatActivity {

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private Button btnLogOut,mEnviarRegistro;
    private int CAMERA_REQUEST_CODE = 0;
    private ProgressDialog progressDialog;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;
    private ImageView imageProfile;
    private EditText  textDatos,textRUC,texttELFONO,textCorreo,textDireccion, textTipoUser;
    private TextView textName;
    private ProgressDialog mProgress;
    public Button btnGoMapa;
    public String keyUser;
    public double latitud, longitud, latitudActual, longitudActual;


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
       /* toolbar.setNavigationIcon(R.drawable.left_arrow); // your drawable
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed(); // Implemented by activity
            }
        });*/

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent intent =  new Intent(PerfilActivity.this,Main2Activity.class);
                startActivity(intent);

            }
        });

        //para que no sea horizontal
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


        textName = (TextView) findViewById(R.id.tvNombre);
        textDatos = (EditText) findViewById(R.id.tvDatosE1);
        textRUC = (EditText) findViewById(R.id.tvRuc1);
        texttELFONO = (EditText) findViewById(R.id.tvTelefono1);
        textCorreo = (EditText) findViewById(R.id.tvCorreo1);
        textDireccion = (EditText) findViewById(R.id.tvDireccion1);
        textTipoUser = (EditText)findViewById(R.id.etTipoUser);


        imageProfile = (ImageView) findViewById(R.id.imageView1);
        imageProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(Intent.createChooser(intent, "Seleccione una imagen de perfil"), CAMERA_REQUEST_CODE);
                }
            }
        });
        btnLogOut = (Button) findViewById(R.id.btn_CancelUpdate);
        btnLogOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mAuth.getCurrentUser() != null)
                    mAuth.signOut();
            }
        });

        progressDialog = new ProgressDialog(this);
        mAuth = FirebaseAuth.getInstance();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    mStorage = FirebaseStorage.getInstance().getReference();
                    mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
                    mDatabase.child(firebaseAuth.getCurrentUser().getUid()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            textName.setText("Bienvenido, "+dataSnapshot.child("name").getValue().toString()+ " !");
                            textDatos.setText(dataSnapshot.child("nameempresa").getValue().toString());
                            textRUC.setText(dataSnapshot.child("ruc").getValue().toString());
                            texttELFONO.setText(dataSnapshot.child("telefono").getValue().toString());
                            textCorreo.setText(dataSnapshot.child("email").getValue().toString());
                            textDireccion.setText(dataSnapshot.child("direccion").getValue().toString());
                            textTipoUser.setText(dataSnapshot.child("tipoUser").getValue().toString());
                            latitudActual=Double.parseDouble(dataSnapshot.child("latitud").getValue().toString()) ;
                            longitudActual=Double.parseDouble(dataSnapshot.child("longitud").getValue().toString());

                            String imageUrl = dataSnapshot.child("image").getValue().toString();
                            if (!imageUrl.equals("default") || TextUtils.isEmpty(imageUrl))
                                Picasso.with(PerfilActivity.this).load(Uri.parse(dataSnapshot.child("image").getValue().toString())).into(imageProfile);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                } else {
                    startActivity(new Intent(PerfilActivity.this, ActivityLoginEmailPass.class));
                    finish();
                }
            }
        };



        mEnviarRegistro = (Button) findViewById(R.id.btnUpdateProfile);

        mProgress = new ProgressDialog(this);
        mEnviarRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startRegister();
                Intent intent = new Intent(PerfilActivity.this, Main2Activity.class);
                startActivity(intent);
                finish();
            }
        });

        Intent intent=getIntent();
        Bundle obtenLatitud=intent.getExtras();
        Bundle obtenLongitud=intent.getExtras();

        if (obtenLatitud!=null && obtenLongitud!=null){

            latitud= obtenLatitud.getDouble("LATITUD");
            longitud= obtenLongitud.getDouble("LONGITUD");

        }

        abrirMapa();

    }

    public void abrirMapa(){

        btnGoMapa = (Button) findViewById(R.id.btnVerMapa);

        // set a onclick listener for when the button gets clicked
        btnGoMapa.setOnClickListener(new View.OnClickListener() {
            // Start new list activity
            public void onClick(View v) {

                Intent mainIntent = new Intent(PerfilActivity.this, MapsActivitySinPerfil.class);
                startActivity(mainIntent);
            }
        });
    }

    public String getRandomString() {
        SecureRandom random = new SecureRandom();
        return new BigInteger(130, random).toString(32);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAMERA_REQUEST_CODE && resultCode == RESULT_OK) {

            if (mAuth.getCurrentUser() == null)
                return;
            progressDialog.setMessage("Actualizando imagen...");
            progressDialog.show();
            final Uri uri = data.getData();
            if (uri == null) {
                progressDialog.dismiss();
                return;
            }
            if (mAuth.getCurrentUser() == null)
                return;

            if (mStorage == null)
                mStorage = FirebaseStorage.getInstance().getReference();
            if (mDatabase == null)
                mDatabase = FirebaseDatabase.getInstance().getReference().child("users");

            final StorageReference filepath = mStorage.child("Photos").child(getRandomString());/*uri.getLastPathSegment()*/
            final DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
            currentUserDB.child("image").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String image = dataSnapshot.getValue().toString();

                    if (!image.equals("default") && !image.isEmpty()) {
                        Task<Void> task = FirebaseStorage.getInstance().getReferenceFromUrl(image).delete();
                        task.addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful())
                                    Toast.makeText(PerfilActivity.this, "Imagen eliminada satisfactoriamente", Toast.LENGTH_SHORT).show();
                                else
                                    Toast.makeText(PerfilActivity.this, "Imagen eliminada fallida", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    currentUserDB.child("image").removeEventListener(this);

                    filepath.putFile(uri).addOnSuccessListener(PerfilActivity.this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            progressDialog.dismiss();
                            Uri downloadUri = taskSnapshot.getDownloadUrl();
                            Toast.makeText(PerfilActivity.this, "Terminado", Toast.LENGTH_SHORT).show();
                            Picasso.with(PerfilActivity.this).load(uri).fit().centerCrop().into(imageProfile);
                            DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
                            currentUserDB.child("image").setValue(downloadUri.toString());
                        }
                    }).addOnFailureListener(PerfilActivity.this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Toast.makeText(PerfilActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private  void  startRegister(){
        final  String preg1 = textDatos.getText().toString().trim();
        final  String preg2 = textRUC.getText().toString().trim();
        final  String preg3 = texttELFONO.getText().toString().trim();
        final  String preg4 = textCorreo.getText().toString().trim();
        final  String preg5 = textDireccion.getText().toString().trim();

        if (!TextUtils.isEmpty(preg1) && !TextUtils.isEmpty(preg2)) {
            mProgress.setMessage("Actualizando Perfil ...");
            mProgress.show();

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
            DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
            currentUserDB.child("nameempresa").setValue(preg1);
            currentUserDB.child("ruc").setValue(preg2);
            currentUserDB.child("telefono").setValue(preg3);
            currentUserDB.child("email").setValue(preg4);
            currentUserDB.child("direccion").setValue(preg5);
            if (latitud!=0 && longitud!=0){
                currentUserDB.child("latitud").setValue(latitud);
                currentUserDB.child("longitud").setValue(longitud);
            }

        }
    }

}
