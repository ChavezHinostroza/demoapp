package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class EditProductActivity extends AppCompatActivity {

    private String productKey;
    private FirebaseAuth mAuth;
    private TextView txtProductName;
    private ImageView imgProduct;
    private TextView productType;
    private TextView productCantidad;
    private TextView txtProductPrecio;
    private TextView txtProductDate;
    private TextView txtProductPrecedencia;
    private Button btnUpdate;
    private ProgressDialog mProgress;
    

    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);

        productKey=getIntent().getExtras().getString("prodKey");
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");

        txtProductName=(TextView)findViewById(R.id.editProdname);
        txtProductPrecio=(TextView)findViewById(R.id.editPrecio);
        txtProductDate=(TextView)findViewById(R.id.editDate);
        //imgProduct=(ImageView)findViewById(R.id.editProdname);
        btnUpdate=(Button)findViewById(R.id.btnUpdateProd) ;

        mProgress = new ProgressDialog(this);

        mDatabase.child(productKey).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String tipoprod_val=(String) dataSnapshot.child("tipoprod").getValue();
                String cantidad_val=(String) dataSnapshot.child("cantidad").getValue();
                String procedencia_val=(String) dataSnapshot.child("procedencia").getValue();
                String image_val = (String) dataSnapshot.child("imageprod").getValue();

                txtProductName.setText(dataSnapshot.child("prodname").getValue().toString());
                txtProductPrecio.setText(dataSnapshot.child("precio").getValue().toString());
                txtProductDate.setText(dataSnapshot.child("fechaor").getValue().toString());

                //Picasso.with(EditProductActivity.this).load(image_val).into(imgProduct);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProduct();
                Intent intent = new Intent(EditProductActivity.this, ProductosByUserActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private void updateProduct(){
        final  String name = txtProductName.getText().toString().trim();
        final  String precio = txtProductPrecio.getText().toString().trim();
        final  String date = txtProductDate.getText().toString().trim();

        if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(precio) && !TextUtils.isEmpty(date)){

            mProgress.setMessage("Guardando Cambios ...");
            mProgress.show();

            DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto").child(productKey);

            mDatabase.child("prodname").setValue(name);
            mDatabase.child("precio").setValue(precio);
            mDatabase.child("fechaor").setValue(date);

        }

    }
}
