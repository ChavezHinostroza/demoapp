package com.example.carlo.demoapp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;


public class OneFragment extends Fragment{

    private RecyclerView mProductList;
    private CardView blanco;
    private LinearLayoutManager mProdLayoutManager;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseBusqueda;
    private DatabaseReference mDatabaseLike;
    private DatabaseReference mDatabaseUsers;

    //GRID

    private GridLayoutManager mItemsLayoutManager;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    private FirebaseUser mCurrentUser;
    FirebaseRecyclerAdapter<Producto, OneFragment.ProductoViewHolder> firebaseRecyclerAdapter;



    public OneFragment() {
        // Required empty public constructor
    }
    //Like procces
    private boolean mProcessLike=false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.fragment_one, container, false);

        mAuth = FirebaseAuth.getInstance();
        mCurrentUser = mAuth.getCurrentUser();
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent Loginintent = new Intent(getActivity(),ActivityRegister2.class);
                    Loginintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(Loginintent);

                }
            }
        };

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Producto");
        mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes").child(mCurrentUser.getUid());
        String key = mDatabase.getKey();

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                //Toast.makeText(getActivity().getApplicationContext(), producto_key,Toast.LENGTH_LONG).show();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        // mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes").child(key);
//.child(firebaseUser.getUid()
        mProductList= (RecyclerView) vista.findViewById(R.id.productlist2);
        mProductList.setHasFixedSize(true);
        mProductList.setLayoutManager(new GridLayoutManager(getContext(),2));

        return vista;
    }

    @Override
    public void onStart() {
        super.onStart();

        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Producto, OneFragment.ProductoViewHolder>(

                Producto.class,
                R.layout.row_productos,
                OneFragment.ProductoViewHolder.class,
                mDatabaseLike

        ) {
            @Override
            protected void populateViewHolder(OneFragment.ProductoViewHolder viewHolder, final Producto model, int position) {

                final String producto_key = getRef(position).getKey();
                final String prodname= model.getProdname();
                final String precio = model.getPrecio();
                final String cantidad = model.getCantidad();
                final String fechaor = model.getFechaor();
                final String procedencia = model.getProcedencia();
                final String tipoprod = model.getTipoprod();
                final String imageprod = model.getImageprod();


                // final String producto_key = getRef(position).getKey();
                viewHolder.setProdname(model.getProdname());
                viewHolder.setTipoprod(model.getTipoprod());
                viewHolder.setPrecio(model.getPrecio());
                viewHolder.setProcedencia(model.getProcedencia());
                viewHolder.setImagenprod(getActivity().getApplicationContext(), model.getImageprod());

                viewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        //Toast.makeText(getContext(), producto_key, Toast.LENGTH_LONG).show();

                        Intent intent = new Intent(getContext(),ProductoSingleActivity.class);
                        intent.putExtra("blog_id",producto_key);
                        getContext().startActivity(intent);
                    }
                });

                //Like

                viewHolder.setLikeBtn(producto_key);
//mostrar on CLikc

                //Like

                viewHolder.mLikeStar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        mProcessLike = true;


                        mDatabaseLike.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                              //  mProcessLike.clear();
                                if(mProcessLike) {

                                    if (dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(producto_key)) {


                                        mProcessLike = false;

                                    }else{
                                        Toast.makeText(getActivity().getApplicationContext(), "QUITÓ PRODUCTO",Toast.LENGTH_LONG).show();
                                        mDatabaseLike.child(mAuth.getCurrentUser().getUid()).child(producto_key).removeValue();
                                        mDatabaseLike.child(producto_key).child("prodname").removeValue();
                                        mDatabaseLike.child(producto_key).child("precio").removeValue();
                                        mDatabaseLike.child(producto_key).child("cantidad").removeValue();
                                        mDatabaseLike.child(producto_key).child("fechaor").removeValue();
                                        mDatabaseLike.child(producto_key).child("procedencia").removeValue();
                                        mDatabaseLike.child(producto_key).child("tipoprod").removeValue();
                                        mDatabaseLike.child(producto_key).child("imageprod").removeValue();



                                        mProductList.removeAllViews();

                                        mProcessLike = false;


                                    }
                                }

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                    }


                });

            }
        };

        //ORDENADA
        // Here you modify your LinearLayoutManager
        mItemsLayoutManager = new GridLayoutManager(getContext(),2);
        //mItemsLayoutManager.setReverseLayout(true);
        //mItemsLayoutManager.setStackFromEnd(true);

        // Now set the layout manager and the adapter to the RecyclerView
        mProductList.setLayoutManager(mItemsLayoutManager);
//ORDENADA CIERRE


        mProductList.setAdapter(firebaseRecyclerAdapter);

    }

    public static class ProductoViewHolder extends RecyclerView.ViewHolder{

        View mView;

        public ProductoViewHolder(View itemView) {
            super(itemView);

            mView=itemView;

            //Para el like
            mLikeStar=(ImageButton) mView.findViewById(R.id.imageButton);
            mDatabaseLike = FirebaseDatabase.getInstance().getReference().child("Likes");
            mAuth =FirebaseAuth.getInstance();

        }

        ImageButton mLikeStar;
        DatabaseReference mDatabaseLike;
        FirebaseAuth mAuth;


        public  void  setProdname(String prodname){
            TextView post_prodname = (TextView) mView.findViewById(R.id.pName);
            post_prodname.setText(prodname);
        }

        public void setTipoprod(String tipoprod){
            TextView post_prodtipo = (TextView) mView.findViewById(R.id.pTipo);
            post_prodtipo.setText(tipoprod);

        }


        public void setPrecio(String precio){
            TextView post_precio = (TextView) mView.findViewById(R.id.pPrecio);
            post_precio.setText(precio);

        }



        public void setProcedencia(String procedencia){
            TextView post_procedencia = (TextView) mView.findViewById(R.id.pProcedencia);
            post_procedencia.setText(procedencia);

        }

        public void setImagenprod(Context ctx, String imageprod){

            ImageView post_imageprod = (ImageView)mView.findViewById(R.id.imgProduct);
            Picasso.with(ctx).load(imageprod).into(post_imageprod);


        }


        public void setLikeBtn(final String producto_key){

            if(mAuth.getCurrentUser()!= null){

                mDatabaseLike.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        if(dataSnapshot.child(mAuth.getCurrentUser().getUid()).hasChild(producto_key)){


                            mLikeStar.setImageResource(R.drawable.remove);

                        }

                        else {

                            mLikeStar.setImageResource(R.drawable.add);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }}


    }

}