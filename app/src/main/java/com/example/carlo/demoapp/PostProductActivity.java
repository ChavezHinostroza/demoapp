package com.example.carlo.demoapp;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;

import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.UploadTask;


/**
 * Created by tapiaalvarez on 18/12/17.
 */

public class PostProductActivity extends AppCompatActivity{

       private ImageView mSelectProdImage;
       private EditText mPostProdname;
       private Spinner mSelectTipoprod;
       private Spinner mSelectCantidad;
       private EditText mPostPrecio;
       private Spinner mSelectProcedencia;
       private EditText mPostCropDate;

       private Button mSubmitBtn;
       private Uri mImageUri=null;

       private static final int GALLERY_REQUEST=1;

       private StorageReference mStorage;
       private DatabaseReference mDatabase,mDatabaseList;

       private ProgressDialog mProgress;


       private FirebaseUser mCurrentUser;
       private DatabaseReference mDatabaseUser;
       private FirebaseAuth.AuthStateListener mAuthListener;
       private FirebaseAuth mAuth;
       FirebaseUser firebaseUser;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.productadd);

        mAuth=FirebaseAuth.getInstance();
        mCurrentUser=mAuth.getCurrentUser();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                    Intent Loginintent = new Intent(PostProductActivity.this,ActivityLoginEmailPass.class);
                    Loginintent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(Loginintent);

                }
            }
        };

        mStorage= FirebaseStorage.getInstance().getReference();
        mDatabase= FirebaseDatabase.getInstance().getReference().child("Producto");
        mDatabaseUser=FirebaseDatabase.getInstance().getReference().child("users2").child(mCurrentUser.getUid());

        mSelectProdImage=(ImageView) findViewById(R.id.addProdimage);
        mPostProdname=(EditText) findViewById(R.id.addProdname);
        mSelectTipoprod=(Spinner) findViewById(R.id.spnTipo);
        mSelectCantidad=(Spinner) findViewById(R.id.spnCantidad);
        mPostPrecio=(EditText) findViewById(R.id.addPrecio);
        mSelectProcedencia=(Spinner) findViewById(R.id.spnProcedencia);
        mPostCropDate=(EditText) findViewById(R.id.addDate);

        mSubmitBtn = (Button) findViewById(R.id.btnSubmitprod);

        mProgress = new ProgressDialog(this);

        mSelectProdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryIntent.setType("image/*");
                startActivityForResult(galleryIntent, GALLERY_REQUEST);
            }});

        mSubmitBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startPosting();
            }
        });}

        private void startPosting(){

            mProgress.setMessage("Procesando..");


            final String prodname_val=mPostProdname.getText().toString().trim();
            final String tipoprod_val=mSelectTipoprod.getSelectedItem().toString();
            final String cantidad_val=mSelectCantidad.getSelectedItem().toString();
            final String precio_val=mPostPrecio.getText().toString().trim();
            final String procedencia_val=mSelectProcedencia.getSelectedItem().toString();
            final String cropdate_val=mPostCropDate.getText().toString().trim();

            if(!TextUtils.isEmpty(prodname_val)&& !TextUtils.isEmpty(tipoprod_val)&&!TextUtils.isEmpty(cantidad_val) &&
                    !TextUtils.isEmpty(precio_val) && !TextUtils.isEmpty(procedencia_val) &&mImageUri!=null
                    && !TextUtils.isEmpty(cropdate_val)){

                mProgress.show();

                StorageReference filepath=mStorage.child("Prod_Images").child(mImageUri.getLastPathSegment());

                filepath.putFile(mImageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        final  Uri downloadUrl=taskSnapshot.getDownloadUrl();

                        final  DatabaseReference newPost=mDatabase.push();



                        mDatabaseUser.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                newPost.child("prodname").setValue(prodname_val);
                                newPost.child("tipoprod").setValue(tipoprod_val);
                                newPost.child("cantidad").setValue(cantidad_val);
                                newPost.child("precio").setValue(precio_val);
                                newPost.child("procedencia").setValue(procedencia_val);
                                newPost.child("fechaor").setValue(cropdate_val);

                                newPost.child("imageprod").setValue(downloadUrl.toString());


                                newPost.child("uid").setValue(mCurrentUser.getUid());

                                newPost.child("name").setValue(dataSnapshot.child("name").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            startActivity(new Intent(PostProductActivity.this, Main2Activity.class));

                                        }

                                    }
                                });

                                newPost.child("telefono").setValue(dataSnapshot.child("telefono").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                          //  startActivity(new Intent(PostProductActivity.this, Main2Activity.class));

                                        }

                                    }
                                });

                                newPost.child("email").setValue(dataSnapshot.child("email").getValue()).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if(task.isSuccessful()){
                                            //  startActivity(new Intent(PostProductActivity.this, Main2Activity.class));

                                        }

                                    }
                                });

                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });

                        mProgress.dismiss();
                        finish();
                    }
                });
            }



        }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==GALLERY_REQUEST &&resultCode==RESULT_OK){

            mImageUri = data.getData();

            mSelectProdImage.setImageURI(mImageUri);

        }
    }


}
