package com.example.carlo.demoapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ActivityRegister2 extends AppCompatActivity {

    private EditText mNameField;
    private EditText mEmailField;
    private EditText mPasswordField, mNombreEmpresa,mTelefono;

    private EditText mPasswordField2;

    private Button btnCancelReg;
    private CardView cvbtn1,cvbtn2;
    private Button mRegisterButton;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Spinner SpTipoUser;

    public boolean valid;

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        mAuth = FirebaseAuth.getInstance();

        //datos
        mNameField = (EditText) findViewById(R.id.etxt_name);
        mEmailField = (EditText) findViewById(R.id.etxt_email);
        mPasswordField = (EditText) findViewById(R.id.etxt_password);

        mPasswordField2=(EditText)findViewById(R.id.etxt_password2);
       // mNombreEmpresa = (EditText) findViewById(R.id.etxt_datosempresa);
       // mTelefono = (EditText) findViewById(R.id.etxt_telefono);
        SpTipoUser = (Spinner)findViewById(R.id.spCountries);


        mRegisterButton = (Button) findViewById(R.id.btn_register);
        cvbtn1 = (CardView)findViewById(R.id.cdbtn3);
        cvbtn1.getBackground().setAlpha(168);

        btnCancelReg = (Button) findViewById(R.id.btn_cancelarreg);
        cvbtn2 = (CardView)findViewById(R.id.cdbtn4);
        cvbtn2.getBackground().setAlpha(168);
        mRegisterButton.getBackground().setAlpha(168);
        btnCancelReg.getBackground().setAlpha(168);

        btnCancelReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivityRegister2.this, ActivityLoginEmailPass.class);
                startActivity(intent);
                finish();
            }
        });

        mProgress =  new ProgressDialog(this);
        mRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startRegister();
            }
        });


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                if (firebaseAuth.getCurrentUser() != null) {
                //    Intent intent = new Intent(ActivityRegister2.this, ActivityLoginEmailPass.class);
                   // startActivity(intent);
                  //  finish();
                }
            }
        };

    }

    private void startRegister() {
        final String name = mNameField.getText().toString().trim();
        final String email = mEmailField.getText().toString().trim();
        final String password = mPasswordField.getText().toString().trim();
        final String password2 = mPasswordField2.getText().toString().trim();
        final String tipoUser = SpTipoUser.getSelectedItem().toString();
        //final String datosempre = mNombreEmpresa.getText().toString().trim();
       // final String telefono = mTelefono.getText().toString().trim();


        validatePass( name, email, password, password2);

        //if (!TextUtils.isEmpty(name) && !TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) //
        if (valid==true){
            mProgress.setMessage("Registrando, por favor espere...");
            mProgress.show();
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            mProgress.dismiss();
                            if (task.isSuccessful()) {

                                sendVerificationEmail();

                                mAuth.signInWithEmailAndPassword(email, password);
                                //Toast.makeText(ActivityRegister.this, user_id, Toast.LENGTH_SHORT).show();

                                DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
                                DatabaseReference currentUserDB = mDatabase.child(mAuth.getCurrentUser().getUid());
                                currentUserDB.child("name").setValue(name);
                                currentUserDB.child("image").setValue("default");
                                currentUserDB.child("ruc").setValue("default");
                                //currentUserDB.child("direccion").setValue("default");
                                currentUserDB.child("latitud").setValue("0");
                                currentUserDB.child("longitud").setValue("0");
                                currentUserDB.child("email").setValue(email);
                                currentUserDB.child("nameempresa").setValue("default");
                                currentUserDB.child("telefono").setValue("default");
                                currentUserDB.child("tipoUser").setValue(tipoUser);


                                Intent intent = new Intent(ActivityRegister2.this, ActivityLoginEmailPass.class);
                                startActivity(intent);

                                Toast.makeText(ActivityRegister2.this, "Registro Finalizado, ahora verifique Email e ingrese al aplicativo!", Toast.LENGTH_SHORT).show();

                            } else
                                Toast.makeText(ActivityRegister2.this, "Error en el registro, Email ya está registrado", Toast.LENGTH_SHORT).show();
                        }


                    });
        }

    }

     public void sendVerificationEmail(){
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if(user!=null){
            user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {
                    if(task.isSuccessful()){

                    }
                    else{
                        Toast.makeText(ActivityRegister2.this, "no se pudo enviar email de verificación",Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

     }


    public void validatePass(String name, String email, String password, String password2){


        /*String name = mNameField.getText().toString();
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();
        String password2 =mPasswordField2.getText().toString().trim();
*/
        if (name.isEmpty() || name.length() < 4) {
            mNameField.setError("al menos 3 caracteres");
            valid = false;
        } else {
            mNameField.setError(null);
            valid = true;
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailField.setError("ingresa un email valido");
            valid = false;
        } else {
            mEmailField.setError(null);
            valid = true;
        }

        if (password.isEmpty() || password.length()<=5) {
            mPasswordField.setText(null);
            mPasswordField.setError("debe contener mínimo 6 valores alfanuméricos");
            valid = false;
        } else {
            mPasswordField.setError(null);
            valid = true;
        }
        if (password2.isEmpty() ||!password2.equals(password)) {
            mPasswordField2.setText(null);
            mPasswordField2.setError("contraseña no coincide");
            valid = false;
        } else {
            mPasswordField2.setText(null);
            mPasswordField2.setError(null);
            valid = true;
        }


    }


}
