package com.example.carlo.demoapp;

import android.*;
import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;

import org.w3c.dom.Text;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity
        implements OnMapReadyCallback, LocationListener, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap mMap;

    private final static int MY_PERMISSION_FINE_LOCATION = 101;
    private LocationManager locationManager1;
    private StorageReference mStorage;
    private DatabaseReference mDatabase;

    public double latitud;
    public double longitud;
    public String direccion;
    public String direccionBD;
    public double latitudBD;
    public double longitudBD;

    public boolean activadormarker;

    public View popup=null;

    private ImageView imageProfile;

    public TextView tv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        locationManager1 = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        locationManager1.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 1, (LocationListener) this);




    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng lima = new LatLng(-12.045925, -77.030546);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(lima));

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            mMap.setMyLocationEnabled(true);

        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSION_FINE_LOCATION);
            }
        }

        mMap.setOnInfoWindowClickListener(this);

        mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @SuppressLint("ResourceType")
            @Override
            public View getInfoContents(Marker marker) {
                if (popup==null){
                    popup=getLayoutInflater().inflate(R.layout.popupmaps,null);
                }
                tv=(TextView)popup.findViewById(R.id.title);

                tv.setText(marker.getTitle());
                tv=(TextView)popup.findViewById(R.id.snippet);
                tv.setText(marker.getSnippet());


                return popup;

            }
        });

        jalarDatos();

    }

    public void enviarSMS(View view) {

        /*try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage("num phono", null, " mensaje aaaaa " + mensaje, null, null);

        }catch (Exception e){
            e.printStackTrace();
        }*/

    }

    public void  marcarEnMapaBD(LatLng lugarBD, String nombreEmpresa, String telefono, String userToken, String imageUrl){

        obtenerDireccionBD(lugarBD);

        MarkerOptions marker= new MarkerOptions().position(lugarBD).title(nombreEmpresa+" - "+telefono)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.icono30px)).snippet(direccionBD);
        if (lugarBD.latitude != 0.0 && lugarBD.longitude != 0.0)
        mMap.addMarker(marker).setTag(userToken);

    }

    public void obtenerDireccionBD(LatLng lugarBD){

        if (lugarBD.latitude != 0.0 && lugarBD.longitude != 0.0) {
            try {
                Geocoder geocoder = new Geocoder(this, Locale.getDefault());
                List<Address> list = geocoder.getFromLocation(lugarBD.latitude, lugarBD.longitude, 1);
                if (!list.isEmpty()) {
                    Address address = list.get(0);
                    direccionBD = address.getAddressLine(0);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public void jalarDatos(){

        mStorage = FirebaseStorage.getInstance().getReference();
        mDatabase = FirebaseDatabase.getInstance().getReference().child("users2");
        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                String userToken=dataSnapshot.getKey();
                String nombreEmpresa=dataSnapshot.child("nameempresa").getValue().toString();
                latitudBD=Double.parseDouble(dataSnapshot.child("latitud").getValue().toString()) ;
                longitudBD=Double.parseDouble(dataSnapshot.child("longitud").getValue().toString()) ;
                String telefono=dataSnapshot.child("telefono").getValue().toString();

                Log.i("MARQUER_TAG",String.valueOf(dataSnapshot.child("image")));
                String imageUrl = dataSnapshot.child("image").toString();
                //if (!imageUrl.equals("default") || TextUtils.isEmpty(imageUrl))
                    //Picasso.with(MapsActivity.this).load(Uri.parse(dataSnapshot.child("image").getValue().toString())).into(imageProfile);

                LatLng lugarBD = new LatLng(latitudBD,longitudBD);

                marcarEnMapaBD(lugarBD, nombreEmpresa, telefono, userToken, imageUrl);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onInfoWindowClick(Marker marker) {

        String token = marker.getTag().toString();

        Intent intentSinglePerfil = new Intent(MapsActivity.this, PerfilSingleActivity.class);
        intentSinglePerfil.putExtra("blog_id",token);
        startActivity(intentSinglePerfil);

        /*

        Toast.makeText(getBaseContext(),
                token,
                Toast.LENGTH_LONG).show();*/
    }
}
