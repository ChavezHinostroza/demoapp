package com.example.carlo.demoapp;

/**
 * Created by tapiaalvarez on 18/12/17.
 */

public class Producto {

    private String prodname, tipoprod, cantidad, precio, fechaor, procedencia, farmername, imageprod,uid,name,telefono,email;

    public Producto(){}


    public Producto(String prodname, String tipoprod, String cantidad, String precio, String fechaor, String procedencia, String farmername, String imageprod, String uid, String name, String telefono, String email) {
        this.prodname = prodname;
        this.tipoprod = tipoprod;
        this.cantidad = cantidad;
        this.precio = precio;
        this.fechaor = fechaor;
        this.procedencia = procedencia;
        this.farmername = farmername;
        this.imageprod = imageprod;
        this.uid = uid;
        this.name = name;
        this.telefono = telefono;
        this.email=email;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTipoprod() {
        return tipoprod;
    }

    public void setTipoprod(String tipoprod) {
        this.tipoprod = tipoprod;
    }

    public String getImageprod() {
        return imageprod;
    }

    public void setImageprod(String imageprod) {
        this.imageprod = imageprod;
    }

    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getFechaor() {
        return fechaor;
    }

    public void setFechaor(String fechaor) {
        this.fechaor = fechaor;
    }

    public String getProcedencia() {
        return procedencia;
    }

    public void setProcedencia(String procedencia) {
        this.procedencia = procedencia;
    }

    public String getFarmername() {
        return farmername;
    }

    public void setFarmername(String farmername) {
        this.farmername = farmername;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
